<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Role extends Model
{
    //
    protected $fillable = ['name'];

    public function add($request){
        $validator = Validator::make($request,[

            'name' => 'unique:roles|required',
        ]);

        if($validator->fails()) {
            return \Response::json(['error'=>$validator->errors()->all()]);
        }

        $this->fill($request);
        if($this->save()){
            return ['status' => 'Role successfully added'];
        }
    }
}
