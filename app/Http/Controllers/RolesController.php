<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Redirect;
use Validator;

class RolesController extends ParentController
{
    public function __construct()
    {

        parent::__construct();

        $this->template = 'roles.index';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $content = view('roles.index_content')->render();
//        dd($content);
        return $this->renderOutPut($content);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $content = view('roles.role_form')->render();
        return $this->renderOutPut($content);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->except('_token'));
        $model = new Role();
        $data = $request->except('_token');
        $result = $model->add($data);
//        dd($result);
        if(is_array($result) && !empty($result['error'])){
            $request->session()->put('status', 'Error');
            return back();
        }

        return redirect(route('roles.index'));
//        ->with($result);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
