<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends ParentController
{
    //

    public function __construct()
    {

        parent::__construct();

        $this->template = 'dashboard.index';
    }

    public function index(){
        $content = view('dashboard.index_content')->render();

        return $this->renderOutPut($content);
    }
}
