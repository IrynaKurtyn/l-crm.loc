<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParentController extends Controller
{
    //
    protected $template = 'layouts.main';
    protected $vars = [];


    public function __construct()
    {

    }

    public function renderOutPut($content = ''){
        $this->vars = array_add($this->vars, 'content', $content);
//        dd(view($this->template)->with($this->vars));
        return view($this->template)->with($this->vars);
    }
}
