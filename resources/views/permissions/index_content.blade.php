<div class="row-fluid">
    {!! Html::link(route('roles.create'),'Add new role',['class' => 'btn btn-success']) !!}
    {!! Html::link(route('permissions.create'),'Add new permission',['class' => 'btn btn-success']) !!}
</div>