
<div class="row-fluid">
    <div class="span10">
        <div class="widget-box">
            <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>New Item creating</h5>
            </div>
            <div class="widget-content nopadding">

                {!! Form::open(['class' => 'form-horizontal' , 'method' => 'post', 'action' => 'RolesController@store', 'enctype' => 'multipart/form-data'])  !!}
                <form action="#" method="get" class="form-horizontal">
                <div class="control-group">
                    {!! Form::label('Name', null, ['class' => 'control-label']); !!}
                    <div class="controls">
                        {!! Form::text('name', null, ['class' => 'span11', 'style' => 'padding: 5px']); !!}
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                </form>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>