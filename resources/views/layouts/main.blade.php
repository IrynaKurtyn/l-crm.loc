<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Material Dashboard by Creative Tim
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    @include('layouts.css')
</head>

<body class="">
<div class="wrapper ">
    @include('layouts.menu')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navigation')
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <!-- End Navbar -->
        <div class="content">
            @yield('content')
        </div>
        @include('leantony::modal.container')
        @include('layouts.footer')
    </div>
</div>
<!--   Core JS Files   -->

@include('layouts.js')
</body>

</html>
