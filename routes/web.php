<?php
use App\Http\Middleware\Checker;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'home']);

Route::resource('users', 'UsersController', ['middleware' => 'checker']);
Route::resource('roles', 'RolesController', ['middleware' => ['checker', 'web']]);
Route::resource('permissions', 'PermissionsController', ['middleware' => ['checker', 'web']]);

Auth::routes();

Route::get('/home', 'DashboardController@index')->name('home');

Route::get('logout', 'Auth\LoginController@logout');